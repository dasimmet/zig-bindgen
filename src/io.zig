const std = @import("std");
const builtin = @import("builtin");

const stdout_fd = std.io.getStdOut();
const stdout_file = stdout_fd.writer();
pub var bw = std.io.bufferedWriter(stdout_file);
pub const stdout = bw.writer();

pub fn print(comptime fmt: anytype, args: anytype) !void {
    if (builtin.os.tag != .freestanding) {
        try stdout.print(fmt, args);
        try bw.flush();
    }
}

pub fn close() void {
    stdout_fd.close();
}
