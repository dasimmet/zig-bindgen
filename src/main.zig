const std = @import("std");
const io = @import("io.zig");
const Wit = @import("wit.zig").Wit;
const Ast = std.zig.Ast;

pub fn main() !void {
    var arena = std.heap.ArenaAllocator.init(std.heap.page_allocator);
    defer arena.deinit();
    var allocator = arena.allocator();

    const argv = try std.process.argsAlloc(allocator);
    if (!(argv.len > 1)) {
        return try io.print("Not Enough Arguments\n", .{});
    }
    const fp = argv[1];
    try io.print("fp: {s}\n", .{fp});

    // var fd = try std.fs.cwd().openFile(fp, .{});

    // const file_contents = try fd.readToEndAllocOptions(allocator, std.math.maxInt(usize), 0, 8, 0);
    // defer allocator.free(file_contents);

    const wit = try Wit.parseFile(allocator, fp);
    // try io.print("AST: {any}\n", .{wit.ast.nodes.items});

    try wit.print(io.stdout);
    try io.bw.flush();

    // var ast = try Ast.parse(allocator, file_contents, .zig);
    // for (ast.nodes.items(.data)) |tok| {
    //     try io.print("{any}: {any}\n", .{ tok, tok });
    // }
}

test "parse a test wit document" {
    var wit = try Wit.parseFile(std.testing.allocator, "libs/wit-bindgen/tests/codegen/char.wit");
    defer wit.deinit(std.testing.allocator);

    try std.testing.expect(wit.ast.nodes.items.len > 1);
    try std.testing.expect(wit.ast.nodes.items[0].node == .Root);

    // defer list.deinit(); // try commenting this out and see if zig detects the memory leak!
}
