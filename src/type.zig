pub inline fn IntSize(
    comptime T: type,
    comptime size: comptime_float,
) type {
    const TI = @typeInfo(T);
    return @Type(std.builtin.Type{ .Int = .{
        .bits = @as(comptime_float, TI.Int.bits) * size,
        .signedness = TI.Int.signedness,
    } });
}
