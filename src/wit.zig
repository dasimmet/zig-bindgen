pub const Wit = @This();
const std = @import("std");
const io = @import("io.zig");
const AST = @import("wit/ast.zig").AST;
const Token = @import("wit/token.zig").Token;
const Tokenizer = @import("wit/token.zig").Tokenizer;

const Parsed = struct {
    ast: *AST,
    file: []const u8,
    pub fn deinit(self: *Parsed, allocator: std.mem.Allocator) void {
        allocator.free(self.file);
        self.ast.deinit();
    }

    pub fn print(self: Parsed, writer: anytype) !void {
        return self.ast.print(writer, self.file);
    }
};

pub fn parseFile(allocator: std.mem.Allocator, p: []const u8) !Parsed {
    var fd = try std.fs.cwd().openFile(p, .{});
    var file_contents = try fd.readToEndAlloc(allocator, std.math.maxInt(usize));
    errdefer allocator.free(file_contents);

    const ast = try parse(allocator, p, file_contents);
    return .{
        .ast = ast,
        .file = file_contents,
    };
}

pub fn parse(allocator: std.mem.Allocator, path: []const u8, file: []const u8) !*AST {
    var gen = Tokenizer{
        .path = path,
        .file = file,
    };

    var tokenList = std.ArrayList(Token).init(allocator);
    defer tokenList.deinit();

    const ast = try Wit.AST.init(allocator);
    errdefer ast.deinit();

    while (gen.next()) |tok| {
        try tokenList.append(tok);
        // try tok.print(file);
        try ast.addToken(tok);
    }
    return ast;
}
