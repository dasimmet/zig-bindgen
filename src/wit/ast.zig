const std = @import("std");
const Token = @import("token.zig").Token;
const CodePoint = @import("codepoint.zig").CodePoint;
const io = @import("../io.zig");
const NodeList = std.ArrayListUnmanaged(Node);

allocator: std.mem.Allocator,
nodes: std.ArrayList(Node),
ctx: *Context,

pub const AST = @This();

pub fn init(allocator: std.mem.Allocator) !*AST {
    var self = try allocator.create(AST);
    errdefer allocator.destroy(self);

    self.allocator = allocator;
    self.nodes = std.ArrayList(Node).init(allocator);
    self.ctx = try allocator.create(Context);
    try self.nodes.append(Node{
        .node = .{ .Root = .{} },
        .p = CodePoint{},
    });
    return self;
}

pub fn deinit(self: *AST) void {
    for (self.nodes.items) |*n| {
        n.deinit(self.allocator);
    }
    self.nodes.deinit();
    self.allocator.destroy(self.ctx);
    self.allocator.destroy(self);
}

pub fn load(self: *AST, token: []Token) !void {
    for (token) |tok| {
        try self.addToken(tok);
    }
}

pub fn root(self: AST) *Root {
    return &self.nodes.items[0].node.Root;
}

pub fn addToken(self: *AST, tok: Token) !void {
    switch (tok.type) {
        .Whitespace => return,
        .Comment => return,
        .Keyword => {
            switch (tok.tag.?.keyword) {
                .default => {
                    self.ctx.default = true;
                },
                .@"export" => {},
                .func => {
                    if (self.ctx.scope != null and self.ctx.scope.?.node == .Function) {
                        var int = self.ctx.interface.?.node.Interface;
                        try int.items.append(self.allocator, .{ .Function = &self.ctx.scope.?.node.Function });
                        try self.nodes.append(self.ctx.scope.?);
                    }
                    self.ctx.scope = Node{
                        .node = .{ .Function = .{
                            .name = "",
                        } },
                        .p = tok.p,
                    };
                },
                .interface => {
                    self.ctx.scope = Node{
                        .node = .{ .Interface = .{
                            .name = "",
                            .default = self.ctx.default,
                            .items = .{},
                        } },
                        .p = tok.p,
                    };
                    self.ctx.default = false;
                },
                .import => {},
                .world => {
                    self.ctx.scope = Node{
                        .node = .{ .World = .{
                            .name = "",
                            .default = self.ctx.default,
                            .items = .{},
                        } },
                        .p = tok.p,
                    };
                    self.ctx.default = false;
                },
                else => |e| {
                    // @compileError("Keyword not Implemented: " ++  ++ "\n");
                    std.debug.print("Keyword not Implemented: {s}\n", .{@tagName(e)});
                    unreachable;
                },
            }
        },
        .Operator => {
            switch (tok.tag.?.operator) {
                .@"{" => {
                    switch (self.ctx.scope.?.node) {
                        .Interface => {
                            self.ctx.interface = self.ctx.scope;
                            self.ctx.interface.?.node.Interface.name = self.ctx.identifier.?;
                            try self.ctx.stack.append(self.allocator, self.ctx.scope.?);
                        },
                        .World => {
                            self.ctx.world = self.ctx.scope;
                            self.ctx.world.?.node.World.name = self.ctx.identifier.?;
                            try self.ctx.stack.append(self.allocator, self.ctx.scope.?);
                        },
                        .Function => {
                            try io.print("Operator not Implemented: {any}\n", .{self.ctx.scope.?.node});
                            unreachable;
                        },
                        else => {
                            try io.print("Operator not Implemented: {any}\n", .{self.ctx.scope.?.node});
                            unreachable;
                        },
                    }
                    self.ctx.scope = null;
                },
                .@"}" => {
                    if (self.ctx.interface) |*n| {
                        n.node.Interface.closed = true;
                        try self.nodes.append(n.*);
                        if (n.node.Interface.default) {
                            self.root().default_interface = &n.node.Interface;
                        }
                        self.ctx.interface = null;
                        // _ = self.ctx.stack.pop();
                    } else if (self.ctx.world) |*n| {
                        n.node.World.closed = true;
                        try self.nodes.append(n.*);
                        try self.root().worlds.append(self.allocator, &n.node.World);
                        if (n.node.World.default) {
                            self.root().default_world = &n.node.World;
                        }
                        self.ctx.world = null;
                    }
                },
                .@"." => {},
                .@":" => {
                    if (self.ctx.expect.arg) {
                        const t = .{
                            .name = self.ctx.identifier.?,
                            .type = undefined,
                        };
                        try self.ctx.scope.?.node.Function.args.append(self.allocator, t);
                    }
                    if (self.ctx.expect.result and self.ctx.scope != null) {
                        const t = .{
                            .name = self.ctx.identifier.?,
                            .type = undefined,
                        };
                        try self.ctx.scope.?.node.Function.results.append(self.allocator, t);
                    }
                },
                .@"(" => {
                    self.ctx.expect.arg = true;
                    self.ctx.scope.?.node.Function.name = self.ctx.identifier.?;
                    self.ctx.identifier = null;
                },
                .@")" => {
                    self.ctx.expect.arg = false;
                },
                .@"->" => {
                    self.ctx.expect.result = true;
                },
                else => {
                    try io.print("Operator not Implemented: {s}\n", .{@tagName(tok.tag.?.operator)});
                    unreachable;
                },
            }
        },
        .Identifier => {
            self.ctx.identifier = tok.tag.?.identifier;
        },
    }
}

pub const Context = struct {
    scope: ?AST.Node = null,
    identifier: ?[]const u8 = null,
    expect: struct {
        arg: bool = false,
        result: bool = false,
    } = .{},
    default: bool = false,
    interface: ?AST.Node = null,
    world: ?AST.Node = null,
    stack: NodeList = .{},
};

pub const Node = struct {
    p: CodePoint,
    node: NodeType,

    pub fn print(self: Node, w: anytype, file: []const u8) !void {
        try std.fmt.format(w, "Node: {s} {s} {any}\n", .{ @tagName(self.node), self.name(), self.p.slice(file) });
    }

    pub fn name(self: Node) []const u8 {
        return switch (self.node) {
            .Root => "root",
            .World => |it| it.name,
            .Interface => |it| it.name,
            .Function => |it| it.name,
            else => @tagName(self.node),
        };
    }

    pub fn deinit(self: *Node, allocator: std.mem.Allocator) void {
        switch (self.node) {
            .Root => |*it| it.deinit(allocator),
            .World => |*it| it.deinit(allocator),
            .Interface => |*it| it.deinit(allocator),
            .Function => |*it| it.deinit(allocator),
            else => {},
        }
    }
};

pub const NodeType = union(enum) {
    Root: Root,
    World: World,
    Interface: Interface,
    Function: Function,
    Export: Export,
    Import: Import,
    Use: Use,
    Typedef: Typedef,
};

pub const Root = struct {
    worlds: std.ArrayListUnmanaged(*World) = .{},
    interfaces: std.ArrayListUnmanaged(*Interface) = .{},
    default_world: ?*World = null,
    default_interface: ?*Interface = null,

    pub fn deinit(self: *@This(), allocator: std.mem.Allocator) void {
        self.worlds.deinit(allocator);
        self.interfaces.deinit(allocator);
    }
};

pub const Import = struct {
    name: []const u8,
};
pub const Export = struct {
    name: []const u8,
};
pub const Use = struct {};

pub const Typedef = Type;
// ;'export' id ':' extern-type
// import-item ::= 'import' id ':' extern-type

pub const InterfaceItems = union(enum) {
    Function: *Function,
    Typedef: *Typedef,
    Use: *Use,
};

pub const WorldItems = union(enum) {
    Export: *Export,
    Import: *Import,
    Use: *Use,
    Typedef: *Typedef,
};

pub const World = struct {
    name: []const u8,
    default: bool = false,
    closed: bool = false,
    items: std.ArrayListUnmanaged(WorldItems) = .{},

    pub fn deinit(self: *@This(), allocator: std.mem.Allocator) void {
        self.items.deinit(allocator);
    }
};

pub const Interface = struct {
    name: []const u8,
    default: bool = false,
    closed: bool = false,
    items: std.ArrayListUnmanaged(InterfaceItems) = .{},

    pub fn deinit(self: *@This(), allocator: std.mem.Allocator) void {
        self.items.deinit(allocator);
    }
};

pub const Type = union(enum) {
    Named: *Named,
    Builtin: Builtin,
    Variant: Variant,
    Record: Record,
    Union: Union,
    Flags: Flags,
    Enum: Enum,
    pub const Named = struct { name: []const u8, type: Type };
    pub const Variant = struct {};
    pub const Record = struct {};
    pub const Union = struct {};
    pub const Flags = struct {};
    pub const Enum = struct {};
    pub const Builtin = enum {
        u8,
        u16,
        u32,
        u64,
        s8,
        s16,
        s32,
        s64,
        float32,
        float64,
        char,
        bool,
        string,
    };
};

pub const Function = struct {
    name: []const u8,
    args: std.ArrayListUnmanaged(Type.Named) = .{},
    results: std.ArrayListUnmanaged(Type.Named) = .{},

    pub fn deinit(self: *@This(), allocator: std.mem.Allocator) void {
        self.args.deinit(allocator);
        self.results.deinit(allocator);
    }
};

pub fn print(self: AST, writer: anytype, file: []const u8) !void {
    for (self.nodes.items) |n| {
        // try io.print("AST: {any}\n", .{n});
        try n.print(writer, file);
    }
}
