pub const CodePoint = @This();

i: usize = 0,
len: usize = 0,
col: usize = 0,
row: usize = 0,
pub fn slice(self: CodePoint, bytes: []const u8) []const u8 {
    return bytes[self.i .. self.i + self.len];
}
