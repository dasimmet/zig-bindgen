const CodePoint = @import("codepoint.zig").CodePoint;
const std = @import("std");
const io = @import("../io.zig");

pub const Tokenizer = struct {
    path: []const u8,
    file: []const u8,
    // pos: IntSize(usize, 64) = 0,
    pos: usize = 0,
    col: usize = 0,
    row: usize = 0,
    pub fn next(self: *Tokenizer) ?Token {
        if (self.comment()) |tok| return tok;
        if (self.keyword(Operator, TokenType.Operator)) |tok| return tok;
        if (self.keyword(Keyword, TokenType.Keyword)) |tok| return tok;
        if (self.charClass(isWhitespace, TokenType.Whitespace)) |tok| return tok;
        if (self.charClass(isIdentifier, TokenType.Identifier)) |tok| return tok;
        if (self.pos >= self.file.len) return null;
        self.print();
        @panic("TODO: Unknown Token");
    }

    fn keyword(self: *Tokenizer, kw: anytype, tokentype: TokenType) ?Token {
        const kfields = @typeInfo(kw).Enum.fields;
        inline for (kfields) |k| {
            const isLongEnough = self.file.len - self.pos > k.name.len;
            if (isLongEnough and std.mem.eql(u8, self.file[self.pos..(self.pos + k.name.len)], k.name)) {
                const posIndex = self.pos;
                const rowIndex = self.row;
                const colIndex = self.col;
                self.pos += k.name.len;
                self.col += k.name.len;
                return Token{
                    .type = tokentype,
                    .p = .{
                        .i = posIndex,
                        .len = self.pos - posIndex,
                        .col = colIndex,
                        .row = rowIndex,
                    },
                    .path = self.path,
                    .tag = Tag.create(@as(kw, @enumFromInt(k.value))),
                };
            }
        }
        return null;
    }

    fn comment(self: *Tokenizer) ?Token {
        const isNoEOF = (self.file.len - self.pos) > 0;
        if (isNoEOF and self.file[self.pos] == '/' and self.file[(self.pos + 1)] == '/') {
            const posIndex = self.pos;
            const rowIndex = self.row;
            const colIndex = self.col;
            while (rowIndex == self.row) {
                self.col += 1;
                if (self.file[self.pos] == '\n') {
                    self.col = 0;
                    self.row += 1;
                }
                self.pos += 1;
            }
            return Token{
                .type = .Comment,
                .p = .{
                    .i = posIndex,
                    .len = self.pos - posIndex,
                    .col = colIndex,
                    .row = rowIndex,
                },
                .path = self.path,
            };
        }
        return null;
    }

    fn charClass(self: *Tokenizer, f: anytype, tokentype: TokenType) ?Token {
        var isNotEOF = self.file.len - self.pos > 0;
        if (isNotEOF and f(self.file[self.pos], 0)) {
            const index = self.pos;
            const rowIndex = self.row;
            const colIndex = self.col;
            while (isNotEOF and f(self.file[self.pos], self.pos - index)) {
                self.col += 1;
                if (self.file[self.pos] == '\n') {
                    self.col = 0;
                    self.row += 1;
                }
                self.pos += 1;
                isNotEOF = self.file.len - self.pos > 0;
            }
            const cp = CodePoint{
                .i = index,
                .len = self.pos - index,
                .col = colIndex,
                .row = rowIndex,
            };
            return Token{
                .type = tokentype,
                .p = cp,
                .tag = .{ .identifier = cp.slice(self.file) },
                .path = self.path,
            };
        }
        return null;
    }

    pub fn isWhitespace(c: u8, i: usize) bool {
        _ = i;
        return std.ascii.isWhitespace(c) or c == '\n';
    }

    pub fn isIdentifier(c: u8, i: usize) bool {
        const isDash = i != 0 and c == '-';
        return std.ascii.isAlphanumeric(c) or isDash;
    }

    fn print(self: Tokenizer) void {
        std.debug.print("{s}:{d}:{d}  char:{c}\n", .{ self.path, self.row, self.col, self.file[self.pos] });
    }
};

pub const Token = struct {
    type: TokenType,
    path: []const u8 = "",
    p: CodePoint = .{},
    tag: ?Tag = null,

    pub fn print(self: Token, bytes: []const u8) !void {
        try io.print("{any} {s}:{d}:{d}\n===\n{s}\n===\n", .{ self.type, self.path, self.p.row + 1, self.p.col + 1, self.p.slice(bytes) });
    }
};

pub const TokenType = enum {
    Comment,
    Whitespace,
    Operator,
    Keyword,
    Identifier,
};

pub const Tag = union(enum) {
    keyword: Keyword,
    operator: Operator,
    identifier: []const u8,
    pub fn create(comptime t: anytype) ?@This() {
        inline for (@typeInfo(@This()).Union.fields) |f| {
            if (f.type == @TypeOf(t)) {
                return @unionInit(@This(), f.name, t);
            }
        }
    }
};

pub const Keyword = enum {
    default,
    @"enum",
    @"export",
    flags,
    func,
    import,
    interface,
    record,
    resource,
    static,
    type,
    @"union",
    use,
    variant,
    world,
};

pub const Operator = enum {
    @"=",
    @",",
    @".",
    @":",
    @";",
    @"(",
    @")",
    @"{",
    @"}",
    @"<",
    @">",
    @"*",
    @"->",
};

pub const keywords = .{
    "use",
    "type",
    "resource",
    "func",
    "record",
    "enum",
    "flags",
    "variant",
    "union",
    "static",
    "interface",
    "world",
    "import",
    "export",
    "default",
};

pub const operators = .{
    "=",
    ",",
    ".",
    ":",
    ";",
    "(",
    ")",
    "{",
    "}",
    "<",
    ">",
    "*",
    "->",
};
